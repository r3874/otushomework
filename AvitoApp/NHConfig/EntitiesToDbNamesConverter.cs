﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AvitoApp.NHConfig
{
    public static class EntitiesToDbNamesConverter
    {
        public static string Convert(string name)
        {
            StringBuilder result = new StringBuilder();
            for (int i = 0; i < name.Length; i++)
            {
                char character = name[index: i];
                if (char.IsUpper(c: character) && i != 0)
                {
                    result.Append(value: '_');
                }

                result.Append(value: character);
            }

            return result.ToString().ToLower();
        }
    }
}
