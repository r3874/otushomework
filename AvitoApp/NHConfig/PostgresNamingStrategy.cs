﻿using NHibernate.Cfg;
using System;

namespace AvitoApp.NHConfig
{
    internal class PostgresNamingStrategy : INamingStrategy
    {
        public string ClassToTableName(string className)
        {
            return EntitiesToDbNamesConverter.Convert(name: className);
        }

        public string PropertyToColumnName(string propertyName)
        {
            return EntitiesToDbNamesConverter.Convert(name: propertyName);
        }

        public string TableName(string tableName)
        {
            return EntitiesToDbNamesConverter.Convert(name: tableName);
        }

        public string ColumnName(string columnName)
        {
            return EntitiesToDbNamesConverter.Convert(name: columnName);
        }

        public string PropertyToTableName(string className, string propertyName)
        {
            return propertyName;
        }

        public string LogicalColumnName(string columnName, string propertyName)
        {
            return String.IsNullOrWhiteSpace(value: columnName) ? propertyName : columnName;
        }
    }
}
