﻿using NHibernate;
using NHibernate.SqlCommand;
using System.Diagnostics;

namespace AvitoApp.NHConfig
{
    public class SqlDebugOutputInterceptor : EmptyInterceptor
    {
        public override SqlString OnPrepareStatement(SqlString sql)
        {
            Debug.Write(message: "NHibernate executes query: ");
            Debug.WriteLine(value: sql);

            return base.OnPrepareStatement(sql: sql);
        }
    }
}
