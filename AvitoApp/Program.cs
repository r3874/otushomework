﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Mapping.Attributes;
using Npgsql;
using AvitoApp.NHConfig;
using System.Reflection;
using AvitoApp.Storage;
using AvitoApp.Entities;
using System.ComponentModel;

namespace AvitoApp
{
    class Program
    {
        static string connectionString;
        private enum UserSimpleAnswer
        {
            Y,
            N
        }

        static List<AvitoApp.Entities.PropertyInfo> categoryTableInfo = new List<AvitoApp.Entities.PropertyInfo>()
        {
            new AvitoApp.Entities.PropertyInfo("Наименование", true, typeof(string), false)
        };

        static List<AvitoApp.Entities.PropertyInfo> userTableInfo = new List<AvitoApp.Entities.PropertyInfo>()
        {
            new AvitoApp.Entities.PropertyInfo("Фамилия", true, typeof(string), false),
            new AvitoApp.Entities.PropertyInfo("Имя", true, typeof(string), false),
            new AvitoApp.Entities.PropertyInfo("Отчество", false, typeof(string), false),
            new AvitoApp.Entities.PropertyInfo("Email", true, typeof(string), false)
        };

        static List<AvitoApp.Entities.PropertyInfo> announcementTableInfo = new List<AvitoApp.Entities.PropertyInfo>()
        {
            new AvitoApp.Entities.PropertyInfo("Тема", true, typeof(string), false),
            new AvitoApp.Entities.PropertyInfo("ИД категории", true, typeof(Int32), true),
            new AvitoApp.Entities.PropertyInfo("ИД автора", true, typeof(Int32), true),
            new AvitoApp.Entities.PropertyInfo("Текст", true, typeof(string), false)
        };

        static List<AvitoApp.Entities.PropertyInfo> messageTableInfo = new List<AvitoApp.Entities.PropertyInfo>()
        {
            new AvitoApp.Entities.PropertyInfo("ИД объявления", true, typeof(Int32), true),
            new AvitoApp.Entities.PropertyInfo("ИД автора", true, typeof(Int32), true),
            new AvitoApp.Entities.PropertyInfo("Текст", true, typeof(string), false)
        };

        static void Main(string[] args)
        {
            CheckDBConnection();

            var menuOption = 0;
            do
            {
                Console.Write("Выберите необходимый пункт меню:\n1. Cоздание таблиц.\n2. Массовое внесение данных в таблицы.\n3. Ручное добавление данных в таблицу.\n4. Показ содержимого таблиц.\n5. Выход.\n\nВвод: ");
                var selectedItem = Console.ReadLine();

                if (Int32.TryParse(selectedItem, out menuOption) && menuOption > 0 && menuOption < 6)
                {
                    switch (menuOption)
                    {
                        case 1:
                            Console.WriteLine("Cоздание таблиц.");
                            try
                            {
                                CreateDBTables();
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(string.Format("Упс. Что-то пошло не так.\nОшибка: {0}\nСтек: {1}", ex.Message, ex.StackTrace));
                                Console.ReadKey();
                                return;
                            }
                            Console.WriteLine("Таблицы созданы успешно.");
                            break;
                        case 2:
                            Console.WriteLine("Массовое внесение данных в таблицы.");
                            try
                            {
                                InsertCategories();
                                InsertAnotherData();
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(string.Format("Упс. Что-то пошло не так.\nОшибка: {0}\nСтек: {1}", ex.Message, ex.StackTrace));
                                Console.ReadKey();
                                return;
                            }
                            break;
                        case 3:
                            Console.WriteLine("Ручное добавление данных в таблицу.");
                            Console.Write("Список таблиц:\n1. Категории объявлений\n2. Пользователи\n3. Объявления\n4. Сообщения\n\nВыберите номер необходимой таблицы: ");
                            var typedTableNumber = Console.ReadLine();
                            var tableNumber = 0;

                            if (Int32.TryParse(typedTableNumber, out tableNumber) && tableNumber > 0 && tableNumber < 5)
                            {
                                try
                                {
                                    InsertRecord(tableNumber);
                                }
                                catch (Exception ex)
                                {
                                    Console.WriteLine(string.Format("Упс. Что-то пошло не так.\nОшибка: {0}\nСтек: {1}", ex.Message, ex.StackTrace));
                                    Console.ReadKey();
                                    return;
                                }
                            }
                            else
                                Console.WriteLine("Неизвестная команда.");
                            break;
                        case 4:
                            Console.WriteLine("Показ содержимого таблиц.");
                            Console.Write("Список таблиц:\n1. Категории объявлений\n2. Пользователи\n3. Объявления\n4. Сообщения\n5. Все\n\nВыберите номер необходимой таблицы: ");
                            typedTableNumber = Console.ReadLine();
                            tableNumber = 0;
                            if (Int32.TryParse(typedTableNumber, out tableNumber) && tableNumber > 0 && tableNumber < 6)
                            {
                                try
                                {
                                    ShowData(tableNumber);
                                }
                                catch (Exception ex)
                                {
                                    Console.WriteLine(string.Format("Упс. Что-то пошло не так.\nОшибка: {0}\nСтек: {1}", ex.Message, ex.StackTrace));
                                    Console.ReadKey();
                                    return;
                                }
                            }
                            else
                                Console.WriteLine("Неизвестная команда.");
                            break;
                        case 5:
                            Console.WriteLine("Для выхода из консоли нажмите любую клавишу.");
                            break;
                        default:
                            break;
                    }
                }
                else
                {
                    menuOption = 5;
                    Console.WriteLine("Неизвестная команда. Завершение работы программы.");
                }

            } while (menuOption != 5);

            Console.ReadKey();
        }

        static ISessionFactory _sessionFactory;
        static ISessionFactory SessionFactory
        {
            get
            {
                if (_sessionFactory == null)
                {
                    Dictionary<string, string> configProperties = new Dictionary<string, string>
                    {
                        {
                            NHibernate.Cfg.Environment.ConnectionDriver,
                            typeof (NHibernate.Driver.NpgsqlDriver).FullName
                        },
                        {
                            NHibernate.Cfg.Environment.Dialect,
                            typeof (NHibernate.Dialect.PostgreSQL82Dialect).FullName
                        },
                        {
                            NHibernate.Cfg.Environment.ConnectionString,
                            connectionString
                        }
                    };

                    HbmSerializer serializer = HbmSerializer.Default;
                    serializer.Validate = true;

                    NHibernate.Cfg.Configuration configuration = new NHibernate.Cfg.Configuration()
                        .SetProperties(newProperties: configProperties)
                        .SetNamingStrategy(newNamingStrategy: new PostgresNamingStrategy())
                        .AddInputStream(xmlInputStream: serializer.Serialize(assembly: Assembly.GetExecutingAssembly()))
                        .SetInterceptor(newInterceptor: new SqlDebugOutputInterceptor());

                    _sessionFactory = configuration.BuildSessionFactory();
                }
                return _sessionFactory;
            }
        }

        // Retrieves a connection string by name.
        // Returns null if the name is not found.
        static string GetConnectionStringByName(string name)
        {
            // Assume failure.
            string returnValue = null;

            // Look for the name in the connectionStrings section.
            ConnectionStringSettings settings =
                ConfigurationManager.ConnectionStrings[name];

            // If found, return the connection string.
            if (settings != null)
                returnValue = settings.ConnectionString;

            return returnValue;
        }

        static void CreateDBTables()
        {
            CreateUsersTable();
            CreateCategoriesTable();
            CreateAnnouncementsTable();
            CreateMessagesTable();
        }

        /// <summary>
        /// Создание таблицы пользователей
        /// </summary>
        static void CreateUsersTable()
        {
            using NpgsqlConnection connection = new NpgsqlConnection(connectionString);
            connection.Open();

            string sql = @"
CREATE SEQUENCE users_id_seq;

CREATE TABLE users
(
    id              INTEGER                     NOT NULL    DEFAULT NEXTVAL('users_id_seq'),
    first_name      CHARACTER VARYING(255)      NOT NULL,
    last_name       CHARACTER VARYING(255)      NOT NULL,
    middle_name     CHARACTER VARYING(255),
    email           CHARACTER VARYING(255)      NOT NULL,
  
    CONSTRAINT users_pkey PRIMARY KEY (id),
    CONSTRAINT users_email_unique UNIQUE (email)
);

CREATE INDEX users_last_name_idx ON users(last_name);
CREATE UNIQUE INDEX users_email_unq_idx ON users(lower(email));
";

            using NpgsqlCommand cmd = new NpgsqlCommand(cmdText: sql, connection: connection);

            string affectedRowsCount = cmd.ExecuteNonQuery().ToString();

            Console.WriteLine(value: $"Created USERS table. Affected rows count: {affectedRowsCount}");
        }

        /// <summary>
        /// Создание таблицы категорий объявлений
        /// </summary>
        static void CreateCategoriesTable()
        {
            using NpgsqlConnection connection = new NpgsqlConnection(connectionString);
            connection.Open();

            string sql = @"
CREATE SEQUENCE categories_id_seq;

CREATE TABLE announcement_categories
(
    id 				integer 				NOT NULL 	DEFAULT nextval('categories_id_seq'),
    name 			character varying(255) 	NOT NULL,
	
    CONSTRAINT categories_pkey PRIMARY KEY (id),
    CONSTRAINT categories_name_unique UNIQUE (name)
);

CREATE INDEX categories_name_idx ON announcement_categories(name);
";

            using NpgsqlCommand cmd = new NpgsqlCommand(cmdText: sql, connection: connection);

            string affectedRowsCount = cmd.ExecuteNonQuery().ToString();

            Console.WriteLine(value: $"Created CATEGORIES table. Affected rows count: {affectedRowsCount}");
        }

        /// <summary>
        /// Создание таблицы объявлений
        /// </summary>
        static void CreateAnnouncementsTable()
        {
            using NpgsqlConnection connection = new NpgsqlConnection(connectionString);
            connection.Open();

            string sql = @"
CREATE SEQUENCE announcements_id_seq;
CREATE TABLE announcements
(
    id              		INTEGER                     	NOT NULL    DEFAULT NEXTVAL('announcements_id_seq'),
    subject      			CHARACTER VARYING(255)      	NOT NULL,
	category_id 			INTEGER							NOT NULL,
	text 					CHARACTER VARYING(2047)     	NOT NULL,
	author_id				INTEGER							NOT NULL,
	create_date_time      	TIMESTAMP WITHOUT TIME ZONE    	NOT NULL,
	status					CHARACTER VARYING(10),   
	close_date_time			TIMESTAMP WITHOUT TIME ZONE,
  
    CONSTRAINT announcements_pkey PRIMARY KEY (id),
	CONSTRAINT announcements_fk_category_id FOREIGN KEY (category_id) REFERENCES announcement_categories(id) ON DELETE RESTRICT,
	CONSTRAINT announcements_fk_author_id FOREIGN KEY (author_id) REFERENCES users(id) ON DELETE RESTRICT
);

CREATE INDEX announcements_author_idx ON announcements(author_id);
CREATE INDEX announcements_subject_idx ON announcements(subject);
";

            using NpgsqlCommand cmd = new NpgsqlCommand(cmdText: sql, connection: connection);

            string affectedRowsCount = cmd.ExecuteNonQuery().ToString();

            Console.WriteLine(value: $"Created ANNOUNCEMENTS table. Affected rows count: {affectedRowsCount}");
        }

        /// <summary>
        /// Создание таблицы сообщений по объявлениям
        /// </summary>
        static void CreateMessagesTable()
        {
            using NpgsqlConnection connection = new NpgsqlConnection(connectionString);
            connection.Open();

            string sql = @"
CREATE SEQUENCE messages_id_seq;

CREATE TABLE messages
(
    id              		INTEGER                     	NOT NULL    DEFAULT NEXTVAL('messages_id_seq'),
	text 					CHARACTER VARYING(511)     		NOT NULL,
	user_id					INTEGER							NOT NULL,
	announcement_id			INTEGER							NOT NULL,
	create_date_time      	TIMESTAMP WITHOUT TIME ZONE    	NOT NULL,
  
    CONSTRAINT messages_pkey PRIMARY KEY (id),
	CONSTRAINT messages_fk_announcement_id FOREIGN KEY (announcement_id) REFERENCES announcements(id) ON DELETE RESTRICT,
	CONSTRAINT messages_fk_user_id FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE RESTRICT
);

CREATE INDEX messages_announcement_idx ON messages(announcement_id);
";

            using NpgsqlCommand cmd = new NpgsqlCommand(cmdText: sql, connection: connection);

            string affectedRowsCount = cmd.ExecuteNonQuery().ToString();

            Console.WriteLine(value: $"Created MESSAGES table. Affected rows count: {affectedRowsCount}");
        }

        static void CheckDBConnection()
        {
            connectionString = GetConnectionStringByName("PostgreSQL");
            if (string.IsNullOrEmpty(connectionString))
            {
                Console.WriteLine("Упс. ConnectionString по имени подключения не найден.");
                Console.ReadKey();
                return;
            }

            try
            {
                using NpgsqlConnection connection = new NpgsqlConnection(connectionString);
                connection.Open();
            }
            catch (Exception ex)
            {
                Console.WriteLine(string.Format("Упс. Проблемы с подключением к серверу БД.\nConnectionString: {0}\nОшибка: {1}\nСтек: {2}",
                    connectionString,
                    ex.Message,
                    ex.StackTrace));
                Console.ReadKey();
                return;
            }
        }

        /// <summary>
        /// Добавление категорий.
        /// </summary>
        static void InsertCategories()
        {
            var storage = new AnnouncementCategoryStorage(sessionFactory: SessionFactory);
            var categories = new List<AnnouncementCategory>()
            {
                new AnnouncementCategory() { Name = "Авто"},
                new AnnouncementCategory() { Name = "Недвижимость"},
                new AnnouncementCategory() { Name = "Услуги"},
                new AnnouncementCategory() { Name = "Электроника"},
                new AnnouncementCategory() { Name = "Животные"}
            };
            storage.SaveAll(categories);
            Console.WriteLine("NHibernate Insert into Announcement Categories table");

            /*List<AnnouncementCategory> categoriesFromDB = storage.GetAll();

            Console.WriteLine($"All announcement categories:\n{string.Join(System.Environment.NewLine, categoriesFromDB.Select(r => string.Format("ID: {0}, Name: {1}", r.Id, r.Name)))}");*/
        }

        static void InsertAnotherData()
        {
            var storage = new AnnouncementCategoryStorage(SessionFactory);
            List<AnnouncementCategory> categories = storage.GetAll();

            #region users
            var userStorage = new UserStorage(SessionFactory);
            User user1 = new User
            {
                FirstName = "Иван",
                LastName = "Иванов",
                MiddleName = "Иванович",
                Email = "ivan@mail.ru"
            };

            User user2 = new User
            {
                FirstName = "Петр",
                LastName = "Петров",
                MiddleName = "Петрович",
                Email = "petr@mail.ru"
            };

            User user3 = new User
            {
                FirstName = "Сергей",
                LastName = "Сергеев",
                MiddleName = "Сергеевич",
                Email = "sergey@mail.ru"
            };

            User user4 = new User
            {
                FirstName = "Павел",
                LastName = "Павлов",
                MiddleName = "Павлович",
                Email = "pavel@mail.ru"
            };

            User user5 = new User
            {
                FirstName = "Денис",
                LastName = "Денисов",
                MiddleName = "Денисович",
                Email = "denis@mail.ru"
            };
            userStorage.SaveAll(new[] { user1, user2, user3, user4, user5 });
            #endregion

            #region announcements

            var announcementStorage = new AnnouncementStorage(SessionFactory);

            Announcement announcement1 = new Announcement
            {
                Author = user1,
                Category = categories.FirstOrDefault(r => r.Name == "Авто"),
                Subject = "Продаю Kia Rio 2016г. на автомате",
                Text = "Продаю Kia Rio 2016г. на автомате.\nДва хозяина по ПТС, все в родном окрасе, по ходовой и силовой без нареканий.",
            };

            Announcement announcement2 = new Announcement
            {
                Author = user1,
                Category = categories.FirstOrDefault(r => r.Name == "Недвижимость"),
                Subject = "Продаю просторный коттедж 250кв.м",
                Text = @"Продаю просторный коттедж 250кв.м.
На территории сауна, беседка, гараж с автоматическими вопротами, септик, в доме все сделано из дорогостоящих материалов.
Продаю в связи с переездом в другой город.",
            };

            Announcement announcement3 = new Announcement
            {
                Author = user2,
                Category = categories.FirstOrDefault(r => r.Name == "Услуги"),
                Subject = "Предлагаю услуги репетитора по математике",
                Text = "Большой опыт работы с детьми с 6 по 11 класс. Подготовка к ЕГЭ и ОГЭ.",
            };

            Announcement announcement4 = new Announcement
            {
                Author = user3,
                Category = categories.FirstOrDefault(r => r.Name == "Электроника"),
                Subject = "Продаю Samsung S22",
                Text = "Память: 256GB, оперативная память: 8GB, цвет: синий. Покупал в 2021, пользовался год, без потертостей и царапин, состояние идеальное.",
            };

            Announcement announcement5 = new Announcement
            {
                Author = user4,
                Category = categories.FirstOrDefault(r => r.Name == "Животные"),
                Subject = "Продаю котят-вислоухих шотландцев",
                Text = "Родились в январе 2022г. К лотку приучены, очень дружные. Продаю сразу всех в одни руки, разделять не хочу.",
            };

            announcementStorage.SaveAll(new[] { announcement1, announcement2, announcement3, announcement4, announcement5 });
            #endregion

            #region messages
            var messageStorage = new MessageStorage(SessionFactory);

            Message message1 = new Message
            {
                Announcement = announcement1,
                User = user2,
                Text = "Добрый день. Торг уместен?"
            };

            Message message2 = new Message
            {
                Announcement = announcement1,
                User = user1,
                Text = "Добрый день. Да, конечно. Также можно провести любую проверку за Ваш счет."
            };

            Message message3 = new Message
            {
                Announcement = announcement3,
                User = user4,
                Text = "Добрый день. Нужно подготовить 10-классника к ЕГЭ. В какие дни Вы свободны?"
            };

            Message message4 = new Message
            {
                Announcement = announcement4,
                User = user5,
                Text = "Здравствуйте. Коробка от телефона и чек у вас на руках?"
            };

            Message message5 = new Message
            {
                Announcement = announcement5,
                User = user3,
                Text = "Здравствуйте. Коробка от телефона и чек у вас на руках?"
            };

            messageStorage.SaveAll(entities: new[] { message1, message2, message3, message4, message5 });
            #endregion

            Console.WriteLine(value: $"NHibernate Insert into USERS, ANNOUNCEMENTS & MESSAGES table");
        }

        static void ShowData(int tableNumber)
        {
            if (tableNumber == 1 || tableNumber == 5)
            {
                var storage = new AnnouncementCategoryStorage(SessionFactory);
                List<AnnouncementCategory> categories = storage.GetAll();
                Console.WriteLine($"Категории объявлений:\n{string.Join(System.Environment.NewLine, categories.Select(r => string.Format("ID: {0}, Name: {1}", r.Id, r.Name)))}");
            }

            if (tableNumber == 2 || tableNumber == 5)
            {
                var userStorage = new UserStorage(SessionFactory);
                List<User> users = userStorage.GetAll();
                Console.WriteLine("\nПользователи:\n" + string.Join(System.Environment.NewLine,
                    users.Select
                    (r => string.Format(@"ID: {0}, LastName: {1}, FirstName: {2}, MiddleName: {3}, Email: {4}",
                    r.Id,
                    r.LastName,
                    r.FirstName,
                    r.MiddleName,
                    r.Email))));
            }

            if (tableNumber == 3 || tableNumber == 5)
            {
                var announcementStorage = new AnnouncementStorage(SessionFactory);
                List<Announcement> announcements = announcementStorage.GetAll();
                Console.WriteLine("\nОбъявления:\n" + string.Join(System.Environment.NewLine,
                    announcements.Select
                    (r => string.Format("ID: {0}\nCategory: {1}\nAuthor: {2}\nSubject: {3}\nText: {4}\nStatus: {5}\nCreateDateTime: {6}\nCloseDateTime: {7}\n",
                    r.Id,
                    r.Category != null ? r.Category.Name : string.Empty,
                    r.Author != null ? r.Author.GetFullName() : string.Empty,
                    r.Subject,
                    r.Text,
                    r.GetStatusTitle(),
                    r.CreateDateTime.ToString(),
                    r.CloseDateTime.HasValue ? r.CloseDateTime.Value.ToString() : string.Empty))));
            }

            if (tableNumber == 4 || tableNumber == 5)
            {
                var messageStorage = new MessageStorage(SessionFactory);
                List<Message> messages = messageStorage.GetAll();
                Console.WriteLine("Сообщения:\n" + string.Join(System.Environment.NewLine,
                    messages.Select
                    (r => string.Format("ID: {0}\nAnnouncement: {1}\nUser: {2}\nText: {3}\nCreateDateTime: {4}\n",
                    r.Id,
                    r.Announcement != null ? r.Announcement.Subject : string.Empty,
                    r.User != null ? r.User.GetFullName() : string.Empty,
                    r.Text,
                    r.CreateDateTime.ToString()))));
            }
        }

        static object Convert(Type type, string input)
        {
            var converter = TypeDescriptor.GetConverter(type);
            if (converter != null)
            {
                // Cast ConvertFromString(string text) : object to (T)
                return converter.ConvertFromString(input);
            }
            return null;
        }

        static void InsertRecord(int tableNumber)
        {
            var tableInfo = new List<AvitoApp.Entities.PropertyInfo>() { };
            switch (tableNumber)
            {
                case 1:
                    tableInfo = categoryTableInfo;
                    break;
                case 2:
                    tableInfo = userTableInfo;
                    break;
                case 3:
                    tableInfo = announcementTableInfo;
                    break;
                case 4:
                    tableInfo = messageTableInfo;
                    break;
                default:
                    break;
            }

            foreach (var property in tableInfo)
            {
                var requiredDataTyped = true;
                do
                {
                    Console.Write(string.Format("{0}{1}: ", property.Name, property.IsRequired ? "*" : string.Empty));
                    var propertyValue = Console.ReadLine();

                    requiredDataTyped = property.IsRequired && !string.IsNullOrEmpty(propertyValue.Trim()) || !property.IsRequired;
                    if (!requiredDataTyped)
                        Console.WriteLine("Свойство обязательно для заполнения!");
                    else
                        property.TypedValue = propertyValue;
                }
                while (!requiredDataTyped);
                property.Value = Convert(property.PropertyType, property.TypedValue);
            }

            var userStorage = new UserStorage(SessionFactory);
            var announcementCategoryStorage = new AnnouncementCategoryStorage(SessionFactory);
            var announcmentStorage = new AnnouncementStorage(SessionFactory);

            switch (tableNumber)
            {
                case 1:
                    var announcementCategory = new AnnouncementCategory();

                    var categoryName = tableInfo.FirstOrDefault(r => r.Name == "Наименование").Value;
                    announcementCategory.Name = categoryName != null ? categoryName.ToString() : string.Empty;

                    announcementCategoryStorage.Save(announcementCategory);
                    Console.WriteLine("Категория добавлена успешно.");
                    break;
                case 2:
                    var user = new User();

                    var lastName = tableInfo.FirstOrDefault(r => r.Name == "Фамилия").Value;
                    user.LastName = lastName != null ? lastName.ToString() : string.Empty;

                    var firstName = tableInfo.FirstOrDefault(r => r.Name == "Имя").Value;
                    user.FirstName = firstName != null ? firstName.ToString() : string.Empty;

                    var middleName = tableInfo.FirstOrDefault(r => r.Name == "Отчество").Value;
                    user.MiddleName = middleName != null ? middleName.ToString() : string.Empty;

                    var email = tableInfo.FirstOrDefault(r => r.Name == "Email").Value;
                    user.Email = email != null ? email.ToString() : string.Empty;

                    userStorage.Save(user);
                    Console.WriteLine("Пользователь добавлен успешно.");
                    break;
                case 3:
                    var announcement = new Announcement();

                    var subject = tableInfo.FirstOrDefault(r => r.Name == "Тема").Value;
                    announcement.Subject = subject != null ? subject.ToString() : string.Empty;

                    var authorIdValue = tableInfo.FirstOrDefault(r => r.Name == "ИД автора").Value;
                    if (authorIdValue != null)
                    {
                        var author = userStorage.Get((int)authorIdValue);
                        announcement.Author = author;
                    }

                    var categoryIdValue = tableInfo.FirstOrDefault(r => r.Name == "ИД категории").Value;
                    if (categoryIdValue != null)
                    {
                        var category = announcementCategoryStorage.Get((int)categoryIdValue);
                        announcement.Category = category;
                    }

                    var text = tableInfo.FirstOrDefault(r => r.Name == "Текст").Value;
                    announcement.Text = text != null ? text.ToString() : string.Empty;

                    announcmentStorage.Save(announcement);

                    Console.WriteLine("Объявление добавлено успешно.");
                    break;
                case 4:
                    var messsageStorage = new MessageStorage(SessionFactory);
                    var message = new Message();

                    var announcementIdValue = tableInfo.FirstOrDefault(r => r.Name == "ИД объявления").Value;
                    if (announcementIdValue != null)
                    {
                        var announcementItem = announcmentStorage.Get((int)announcementIdValue);
                        message.Announcement = announcementItem;
                    }

                    var userIdValue = tableInfo.FirstOrDefault(r => r.Name == "ИД автора").Value;
                    if (userIdValue != null)
                    {
                        var userItem = userStorage.Get((int)userIdValue);
                        message.User = userItem;
                    }

                    var messageText = tableInfo.FirstOrDefault(r => r.Name == "Текст").Value;
                    message.Text = messageText != null ? messageText.ToString() : string.Empty;

                    messsageStorage.Save(message);

                    Console.WriteLine("Сообщение добавлено успешно.");
                    break;
                default:
                    break;
            }
        }
    }
}
