﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AvitoApp.Entities
{
    class PropertyInfo
    {
        public string Name { get; set; }
        public bool IsRequired { get; set; }
        public bool IsFK { get; set; }
        public string TypedValue { get; set; }
        public object Value { get; set; }
        public Type PropertyType { get; set;}
        public PropertyInfo(string name, bool isRequired, Type propertyType, bool isFK)
        {
            this.Name = name;
            this.IsRequired = isRequired;
            this.IsFK = IsFK;
            this.PropertyType = propertyType;
        }
    }
}
