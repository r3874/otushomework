﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate.Mapping.Attributes;

namespace AvitoApp.Entities
{
    [Class(Table = "users")]
    public class User
    {
        [Id(position: 0, Name = "Id")]
        [Generator(position: 1, Class = "native")]
        public virtual long Id { get; set; }

        [Property(NotNull = true)]
        public virtual string FirstName { get; set; }

        [Index(position: 0)]
        [Property(position: 1, NotNull = true, Index = "users_last_name_idx")]
        public virtual string LastName { get; set; }

        [Property]
        public virtual string MiddleName { get; set; }

        [Property(NotNull = true, Unique = true, UniqueKey = "users_email_unique")]
        public virtual string Email { get; set; }

        [Bag(position: 0, Name = "Announcements", Inverse = true)]
        [Key(position: 1, Column = "AuthorId")]
        [OneToMany(position: 2, ClassType = typeof(Announcement))]
        public virtual IList<Announcement> Announcements { get; set; }

        [Bag(position: 0, Name = "Messages", Inverse = true)]
        [Key(position: 1, Column = "UserId")]
        [OneToMany(position: 2, ClassType = typeof(Message))]
        public virtual IList<Message> Messages { get; set; }

        public virtual string GetFullName()
        {
            return $"{this.LastName} {this.FirstName} {this.MiddleName}";
        }
    }
}
