﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate.Mapping.Attributes;

namespace AvitoApp.Entities
{
    [Class(Table = "messages")]
    public class Message
    {
        [Id(position: 0, Name = "Id")]
        [Generator(position: 1, Class = "native")]
        public virtual long Id { get; set; }

        [Property(NotNull = true)]
        public virtual string Text { get; set; }

        
        [ManyToOne(0, Column = "user_id", ForeignKey = "messages_fk_user_id", Cascade = "all", Lazy = Laziness.False)]
        [Property(1, NotNull = true)]
        public virtual User User { get; set; }

        [ManyToOne(0, Column = "announcement_id", ForeignKey = "messages_fk_announcement_id", Cascade = "all", Lazy = Laziness.False)]
        [Index(position: 1)]
        [Property(position: 2, NotNull = true, Index = "messages_announcement_idx")]

        public virtual Announcement Announcement { get; set; }

        [Property(NotNull = true)]
        public virtual DateTime CreateDateTime { get; set; }
    }
}
