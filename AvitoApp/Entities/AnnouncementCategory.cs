﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate.Mapping.Attributes;

namespace AvitoApp.Entities
{
    [Class(Table = "announcement_categories")]
    public class AnnouncementCategory
    {
        [Id(position: 0, Name = "Id")]
        [Generator(position: 1, Class = "native")]
        public virtual long Id { get; set; }

        [Index(position: 0)]
        [Property(position: 1, NotNull = true, Index = "categories_name_idx", Unique = true, UniqueKey = "categories_name_unique")]
        public virtual string Name { get; set; }

        [Bag(position: 0, Name = "Announcements", Inverse = true)]
        [Key(position: 1, Column = "CategoryId")]
        [OneToMany(position: 2, ClassType = typeof(Announcement))]
        public virtual IList<Announcement> Announcements { get; set; }
    }
}
