﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate.Mapping.Attributes;

namespace AvitoApp.Entities
{
    [Class(Table = "announcements")]
    public class Announcement
    {
        [Id(position: 0, Name = "Id")]
        [Generator(position: 1, Class = "native")]
        public virtual long Id { get; set; }

        [Index(position: 0)]
        [Property(position: 1, NotNull = true, Index = "announcements_subject_idx")]
        public virtual string Subject { get; set; }

        [ManyToOne(0, Column = "category_id", ForeignKey = "announcements_fk_category_id", Cascade = "all", Lazy = Laziness.False)]
        [Property(1, NotNull = true)]
        public virtual AnnouncementCategory Category { get; set; }

        [Property(NotNull = true)]
        public virtual string Text { get; set; }

        [ManyToOne(0, Column = "author_id", ForeignKey = "announcements_fk_author_id", Cascade = "all", Lazy = Laziness.False)]
        [Index(1)]
        [Property(2, Index = "announcements_author_idx", NotNull = true)]
        public virtual User Author { get; set; }

        [Property(NotNull = true)]
        public virtual DateTime CreateDateTime { get; set; }

        [Property]
        public virtual string Status { get; set; }

        [Property]
        public virtual DateTime? CloseDateTime { get; set; }

        [Bag(position: 0, Name = "Messages", Inverse = true)]
        [Key(position: 1, Column = "AnnouncementId")]
        [OneToMany(position: 2, ClassType = typeof(Message))]
        public virtual IList<Message> Messages { get; set; }

        public virtual string GetStatusTitle()
        {
            var statusTitle = string.Empty;

            switch (this.Status)
            {
                case "active":
                    statusTitle = "Активно";
                    break;
                case "completed":
                    statusTitle = "Завершено";
                    break;
                case "closed":
                    statusTitle = "Закрыто";
                    break;
                default:
                    break;
            };

            return statusTitle;
        }
    }

    enum Status
    {
        active,
        completed,
        closed
    }
}
