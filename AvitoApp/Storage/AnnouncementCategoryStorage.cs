﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AvitoApp.Entities;
using NHibernate;

namespace AvitoApp.Storage
{
    class AnnouncementCategoryStorage
    {
        private readonly ISessionFactory _sessionFactory;

        public AnnouncementCategoryStorage(ISessionFactory sessionFactory)
        {
            this._sessionFactory = sessionFactory;
        }
        public void SaveAll(IEnumerable<AnnouncementCategory> entities)
        {
            using (ISession session = this._sessionFactory.OpenSession())
            {
                using (ITransaction tx = session.BeginTransaction())
                {
                    foreach (AnnouncementCategory entity in entities)
                    {
                        if (!session.Query<AnnouncementCategory>().Any(r => r.Id != entity.Id && r.Name == entity.Name))
                            session.SaveOrUpdate(entity);
                    }
                    tx.Commit();
                }
            }
        }

        public List<AnnouncementCategory> GetAll()
        {
            using (ISession session = this._sessionFactory.OpenSession())
            {
                using (ITransaction tx = session.BeginTransaction())
                {
                    return session.Query<AnnouncementCategory>()
                        .OrderBy(keySelector: it => it.Name)
                        .ToList();
                }
            }
        }

        public AnnouncementCategory Get(int Id)
        {
            if (Id <= 0)
                return null;

            using (ISession session = this._sessionFactory.OpenSession())
            {
                using (ITransaction tx = session.BeginTransaction())
                {
                    return session.Query<AnnouncementCategory>().FirstOrDefault(r => r.Id == Id);
                }
            }
        }

        public void SaveOrUpdate(AnnouncementCategory entity)
        {
            using (ISession session = this._sessionFactory.OpenSession())
            {
                using (ITransaction tx = session.BeginTransaction())
                {
                    session.SaveOrUpdate(obj: entity);
                    tx.Commit();
                }
            }
        }

        public void Save(AnnouncementCategory entity)
        {
            using (ISession session = this._sessionFactory.OpenSession())
            {
                using (ITransaction tx = session.BeginTransaction())
                {
                    session.Save(obj: entity);
                    tx.Commit();
                }
            }
        }
    }
}
