﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AvitoApp.Entities;
using NHibernate;

namespace AvitoApp.Storage
{
    class AnnouncementStorage
    {
        private readonly ISessionFactory _sessionFactory;

        public AnnouncementStorage(ISessionFactory sessionFactory)
        {
            this._sessionFactory = sessionFactory;
        }
        public void SaveAll(IEnumerable<Announcement> entities)
        {
            using (ISession session = this._sessionFactory.OpenSession())
            {
                using (ITransaction tx = session.BeginTransaction())
                {
                    foreach (Announcement entity in entities)
                    {
                        if (entity.Id == 0)
                        {
                            entity.CreateDateTime = DateTime.UtcNow;
                            entity.Status = Status.active.ToString();
                        }
                        else
                        {
                            var announcement = session.Query<Announcement>().FirstOrDefault(r => r.Id == entity.Id);
                            if (announcement != null)
                            {
                                if (announcement.Status == Status.active.ToString() && (entity.Status == Status.closed.ToString() || entity.Status == Status.completed.ToString()))
                                    entity.CloseDateTime = DateTime.UtcNow;
                                else if ((announcement.Status == Status.closed.ToString() || announcement.Status == Status.completed.ToString()) && entity.Status == Status.active.ToString())
                                    entity.CloseDateTime = null;
                            }
                        }

                        session.Save(entity);
                    }
                    tx.Commit();
                }
            }
        }

        public List<Announcement> GetAll()
        {
            using (ISession session = this._sessionFactory.OpenSession())
            {
                using (ITransaction tx = session.BeginTransaction())
                {
                    return session.Query<Announcement>()
                        .OrderBy(it => it.CreateDateTime)
                        .ToList();
                }
            }
        }

        public Announcement Get(int Id)
        {
            if (Id <= 0)
                return null;

            using (ISession session = this._sessionFactory.OpenSession())
            {
                using (ITransaction tx = session.BeginTransaction())
                {
                    return session.Query<Announcement>().FirstOrDefault(r => r.Id == Id);
                }
            }
        }

        public void SaveOrUpdate(Announcement entity)
        {
            using (ISession session = this._sessionFactory.OpenSession())
            {
                using (ITransaction tx = session.BeginTransaction())
                {
                    session.SaveOrUpdate(obj: entity);
                    tx.Commit();
                }
            }
        }

        public void Save(Announcement entity)
        {
            using (ISession session = this._sessionFactory.OpenSession())
            {
                using (ITransaction tx = session.BeginTransaction())
                {
                    if (entity.Id == 0)
                    {
                        entity.CreateDateTime = DateTime.UtcNow;
                        entity.Status = Status.active.ToString();
                    }
                    else
                    {
                        var announcement = session.Query<Announcement>().FirstOrDefault(r => r.Id == entity.Id);
                        if (announcement != null)
                        {
                            if (announcement.Status == Status.active.ToString() && (entity.Status == Status.closed.ToString() || entity.Status == Status.completed.ToString()))
                                entity.CloseDateTime = DateTime.UtcNow;
                            else if ((announcement.Status == Status.closed.ToString() || announcement.Status == Status.completed.ToString()) && entity.Status == Status.active.ToString())
                                entity.CloseDateTime = null;
                        }
                    }
                    session.Save(obj: entity);
                    tx.Commit();
                }
            }
        }
    }
}
