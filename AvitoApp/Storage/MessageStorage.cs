﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AvitoApp.Entities;
using NHibernate;

namespace AvitoApp.Storage
{
    class MessageStorage
    {
        private readonly ISessionFactory _sessionFactory;

        public MessageStorage(ISessionFactory sessionFactory)
        {
            this._sessionFactory = sessionFactory;
        }
        public void SaveAll(IEnumerable<Message> entities)
        {
            using (ISession session = this._sessionFactory.OpenSession())
            {
                using (ITransaction tx = session.BeginTransaction())
                {
                    foreach (Message entity in entities)
                    {
                        if (!session.Query<Message>()
                            .Any(r => r.Id == entity.Id &&
                            ((r.Announcement != null && entity.Announcement != null && r.Announcement.Id != entity.Announcement.Id) ||
                            (r.User != null && entity.User != null && r.User.Id != entity.User.Id))))
                        {
                            if (entity.Id == 0)
                                entity.CreateDateTime = DateTime.UtcNow;
                            else
                            {
                                var message = session.Query<Message>().FirstOrDefault(r => r.Id == entity.Id);
                                if (message != null && !Equals(entity.CreateDateTime, message.CreateDateTime))
                                    entity.CreateDateTime = message.CreateDateTime;
                            }
                            session.SaveOrUpdate(entity);
                        }
                    }
                    tx.Commit();
                }
            }
        }

        public List<Message> GetAll()
        {
            using (ISession session = this._sessionFactory.OpenSession())
            {
                using (ITransaction tx = session.BeginTransaction())
                {
                    return session.Query<Message>()
                        .OrderBy(keySelector: it => it.Id)
                        .ToList();
                }
            }
        }

        public void SaveOrUpdate(Message entity)
        {
            using (ISession session = this._sessionFactory.OpenSession())
            {
                using (ITransaction tx = session.BeginTransaction())
                {
                    session.SaveOrUpdate(obj: entity);
                    tx.Commit();
                }
            }
        }

        public void Save(Message entity)
        {
            using (ISession session = this._sessionFactory.OpenSession())
            {
                using (ITransaction tx = session.BeginTransaction())
                {
                    if (entity.Id == 0)
                        entity.CreateDateTime = DateTime.UtcNow;
                    else
                    {
                        var message = session.Query<Message>().FirstOrDefault(r => r.Id == entity.Id);
                        if (message != null && !Equals(entity.CreateDateTime, message.CreateDateTime))
                            entity.CreateDateTime = message.CreateDateTime;
                    }
                    session.Save(obj: entity);
                    tx.Commit();
                }
            }
        }
    }
}
