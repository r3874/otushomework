﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AvitoApp.Entities;
using NHibernate;

namespace AvitoApp.Storage
{
    class UserStorage
    {
        private readonly ISessionFactory _sessionFactory;

        public UserStorage(ISessionFactory sessionFactory)
        {
            this._sessionFactory = sessionFactory;
        }
        public void SaveAll(IEnumerable<User> entities)
        {
            using (ISession session = this._sessionFactory.OpenSession())
            {
                using (ITransaction tx = session.BeginTransaction())
                {
                    foreach (User entity in entities)
                    {
                        if (!session.Query<User>().Any(r => r.Id != entity.Id && r.Email == entity.Email.Trim().ToLower()))
                        {
                            if (entity.Id == 0)
                                entity.Email = entity.Email.Trim().ToLower();

                            session.SaveOrUpdate(entity);
                        }
                    }
                    tx.Commit();
                }
            }
        }

        public List<User> GetAll()
        {
            using (ISession session = this._sessionFactory.OpenSession())
            {
                using (ITransaction tx = session.BeginTransaction())
                {
                    return session.Query<User>()
                        .OrderBy(keySelector: it => it.Id)
                        .ToList();
                }
            }
        }

        public User Get(int Id)
        {
            if (Id <= 0)
                return null;

            using (ISession session = this._sessionFactory.OpenSession())
            {
                using (ITransaction tx = session.BeginTransaction())
                {
                    return session.Query<User>().FirstOrDefault(r => r.Id == Id);
                }
            }
        }

        public void SaveOrUpdate(User entity)
        {
            using (ISession session = this._sessionFactory.OpenSession())
            {
                using (ITransaction tx = session.BeginTransaction())
                {
                    session.SaveOrUpdate(obj: entity);
                    tx.Commit();
                }
            }
        }

        public void Save(User entity)
        {
            using (ISession session = this._sessionFactory.OpenSession())
            {
                using (ITransaction tx = session.BeginTransaction())
                {
                    if (entity.Id == 0)
                        entity.Email = entity.Email.Trim().ToLower();

                    session.Save(obj: entity);
                    tx.Commit();
                }
            }
        }
    }
}
